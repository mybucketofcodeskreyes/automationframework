@web
Feature: web test one using Google

  Perform simple search and verify actions on a google page.


  @search
  Scenario: Perform a google match for a specified term
    Given I want to navigate to the URL "https://www.google.com"
    When I will search for the term "Happiness"
    Then I should see there is a search result with the text "Happy" and "happiness"