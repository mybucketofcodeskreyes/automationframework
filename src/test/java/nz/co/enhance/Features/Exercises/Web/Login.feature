@login
Feature: Logging in to Trade me website using valid and invalid credentials

  User to login to Trade me website

  @multipleLogin
  Scenario Outline: Valid and invalid login credentials
    Given I navigate to the login page https://www.trademe.co.nz
    When I click the login button
    When I login using "<email>" as email and "<password>" as password
    Then I should verify the page contains "<expectedResult>"
    Examples:
      | email                          | password        | expectedResult                                   |
      | ramirez.karla.abalos@gmail.com | kar022018       | Logout                                           |
      | test54321@gmail.com            | password        | The email or password you entered is incorrect.  |