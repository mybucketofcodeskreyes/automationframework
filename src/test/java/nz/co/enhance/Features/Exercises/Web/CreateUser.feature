@register
Feature: Creating a new user in Trade me website

  To create a new user in Trade me website

  @registerIndividualUser
  Scenario: Create a new user for Trade me site
    Given I navigate to this url https://www.trademe.co.nz/
    When I register
    When fill in the required information
    Then I should be able to create my account successfully