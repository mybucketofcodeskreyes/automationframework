@api
Feature: Examples of creating Rest - Get requests and parsing JSON

  Scenario: Send a GET request and parse the response in reqres.in site ans assert multiple data
    When I send a GET request to test api with multiple assertion
    Then I should see a response code which is 200
    And I should see the response contains multiple data:
      | node | fieldName      | expectedResult                                                      |
      | 0    | id             | 4                                                                   |
      | 0    | first_name     | Eve                                                                 |
      | 0    | last_name      | Holt                                                                |
      | 0    | avatar         | https://s3.amazonaws.com/uifaces/faces/twitter/marcoramires/128.jpg |
      | 1    | id             | 5                                                                   |
      | 1    | first_name     | Charles                                                             |
      | 1    | last_name      | Morris                                                              |
      | 1    | avatar         | https://s3.amazonaws.com/uifaces/faces/twitter/stephenmoon/128.jpg  |
      | 2    | id             | 6                                                                   |
      | 2    | first_name     | Tracey                                                              |
      | 2    | last_name      | Ramos                                                               |
      | 2    | avatar         | https://s3.amazonaws.com/uifaces/faces/twitter/bigmancho/128.jpg       |
