@api
Feature: Examples of creating Rest - Get requests and parsing JSON

  Scenario: Send a successful GET request and parse the response in reqres.in site
    When I send a GET request to test api
    Then I should see a response code is 200
    And I should see the response contains the following data:
      | fieldName     | expectedResult                     |
      | id            | 2                                  |
      | name          | fuchsia rose                       |
      | year          | 2001                               |
      | color         | #C74375                            |
      | pantone_value | 17-2031                            |

  Scenario: Send an unsuccessful GET request and parse the response in reqres.in site
    When I send a GET request to test api
    Then I should see a response code is 200
    And I should see the response contains the following data:
      | fieldName     | expectedResult                     |
      | id            | 2                                  |
      | name          | fuchsia rose                             |
      | year          | 2001                               |
      | color         | #C74375                            |
      | pantone_value | 17-2031                            |
