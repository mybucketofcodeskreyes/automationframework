#  Mobile Browser Web Exercise
#
# Web tests on mobile are very similar to a regular browser except for the capabilities and setup to make the tests run on an emulator.
#  The way we write them is the same as web, but be aware that often responsive websites will use hamburger menus or similar
#  to enable the user to navigate so you may need to modify your page objects to cope with both regular browser and mobile browser.
#
#  Exercise:
#  - Create mobile versions of the Trademe web tests you have already created.
#  - Create two or three new scenarios that perform actions specific to mobile web
