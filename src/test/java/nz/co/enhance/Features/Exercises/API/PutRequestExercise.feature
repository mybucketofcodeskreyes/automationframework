@api
Feature: Examples of creating Rest - Put requests and parsing JSON

  Scenario: Send a successful PUT request and parse the response in reqres.in site
    When I send a PUT request to test api
    Then I should see a PUT response code of 200
    And I should see the PUT response contains text updatedAt
    And the following fields name and job should have the following values:
      | fieldName     | expectedResult                     |
      | name          | morpheus                           |
      | job           | zion resident                      |