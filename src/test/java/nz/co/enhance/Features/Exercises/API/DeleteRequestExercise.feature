@api
Feature: Examples of creating Rest - Delete requests and parsing JSON

  Scenario: Send a successful Delete request and parse the response in reqres.in site
    Given I send a Delete request to test api for user 2
    Then I should see a Delete response code of 204
    And I should see the  response with empty value
