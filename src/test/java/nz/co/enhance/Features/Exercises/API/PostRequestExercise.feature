@api
Feature: Examples of creating Rest - Post requests and parsing JSON

  Scenario: Create a successful POST request using objects
    When I send a simple POST request with the following data:
      | fieldName | value      |
      | name      | morpheus   |
      | job       | leader     |
    Then I should see the Post response code is 201
    And I should verify the Post response contains text id and createdAt

