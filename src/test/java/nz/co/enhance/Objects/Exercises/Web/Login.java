package nz.co.enhance.Objects.Exercises.Web;

import nz.co.enhance.automationFramework.Element;
import org.openqa.selenium.By;

public class Login {
    private Element email = new Element(By.id("page_email"));
    private Element password = new Element(By.id("page_password"));
    private Element loginBtn = new Element(By.id("LoginPageButton"));

    public Login() {
        email.waitForElementToBeDisplayed(5);
    }

    public void toLoginUsingCredentials(String emailAd, String pw){
        email.sendKeys(emailAd);
        password.sendKeys(pw);
        loginBtn.click();
    }
}
