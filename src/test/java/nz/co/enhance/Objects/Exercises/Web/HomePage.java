package nz.co.enhance.Objects.Exercises.Web;

import nz.co.enhance.automationFramework.Element;
import org.openqa.selenium.By;

public class HomePage {
    public Element logo = new Element(By.id("SiteHeader_SiteTabs_kevin"));
    private Element linkTxtRegister = new Element(By.linkText("Register"));
    private Element btnLogin = new Element(By.id("LoginLink"));

    public HomePage() {
        logo.waitForElementToBeDisplayed(10);
    }

    public void toRegister() {
        linkTxtRegister.click();
    }

    public void toLogin() {
        btnLogin.click();
    }
}
