package nz.co.enhance.Objects.Exercises.Web;

import nz.co.enhance.automationFramework.Element;
import org.openqa.selenium.By;

public class CreateAccount {

    public Element addressSelection;

    public CreateAccount(String address){
         email.waitForElementToBeDisplayed(5);
         addressSelection = new Element(By.xpath("//div[@class='address-suggestions-115']/ul/li/span[contains(text(),'" + address + "')]")); // to pass the address value from stepdef
    }

    public Element email = new Element(By.id("LoginDetails_EmailAddressTextBox"));
    public Element password = new Element(By.id("LoginDetails_PasswordTextBox"));
    public Element confirmPassword = new Element(By.id("LoginDetails_ConfirmPasswordTextBox"));
    public Element username = new Element(By.id("LoginDetails_UserNameTextBox"));
    public Element firstname = new Element(By.id("ContactDetails_FirstNameTextBox"));
    public Element lastname = new Element(By.id("ContactDetails_LastNameTextBox"));
    public Element gender = new Element(By.id("ContactDetails_GenderFemale"));
    public Element dobDay = new Element(By.id("ContactDetails_DobDay"));
    public Element dobMonth = new Element(By.id("ContactDetails_DobMonth"));
    public Element dobYear = new Element(By.id("ContactDetails_DobYear"));
    public Element phone = new Element(By.id("ContactDetails_ContactPhoneTextBox"));
    public Element country = new Element(By.id("ContactDetails_StreetAddress_radioNewZealand"));
    public Element addressFinder = new Element(By.className("js-txt-search-address"));
    public Element postCode = new Element(By.id("ContactDetails_StreetAddress_PostcodeTextBox"));
    public Element closestDistrict = new Element(By.id("ContactDetails_ClosestSuburbDropDown"));
    public Element terms = new Element(By.cssSelector("input[name='TnCCheckbox']"));
    public Element submit = new Element(By.id("SubmitButton"));
}
