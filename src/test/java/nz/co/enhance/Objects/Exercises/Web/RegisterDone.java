package nz.co.enhance.Objects.Exercises.Web;

import nz.co.enhance.TestDriver;
import nz.co.enhance.automationFramework.Element;
import org.openqa.selenium.By;


public class RegisterDone {
    public Element welcomeMsg = new Element(By.id("mainContent"));

    public RegisterDone() {
      welcomeMsg.waitForElementToBeDisplayed(30);
    }
}
