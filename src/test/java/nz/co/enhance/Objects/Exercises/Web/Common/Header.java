package nz.co.enhance.Objects.Exercises.Web.Common;

import nz.co.enhance.TestDriver;
import nz.co.enhance.automationFramework.Element;
import org.openqa.selenium.By;

public class Header {
    public Element logo = new Element(By.id("SiteHeader_SiteTabs_kevin"));

    public Header() {
        logo.waitForElementToBeDisplayed(10);
    }
}