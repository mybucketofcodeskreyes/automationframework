package nz.co.enhance.Objects.Examples;

import nz.co.enhance.automationFramework.Element;
import org.openqa.selenium.By;

public class GoogleSearchPage {

    public Element searchBox = new Element(By.name("q"));
    public Element searchResults = new Element(By.id("search"));

    // We usually instantiate the page with a check to see if a unique element has loaded before we continue.
    public GoogleSearchPage() {
        //this will poll the searchBox for up to 20 seconds before the test fails.
        searchBox.waitForElementToBeDisplayed(20);
    }


}
