package nz.co.enhance.StepDefs;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import nz.co.enhance.TestDriver;
import nz.co.enhance.automationFramework.HelperClasses.HelperMethods;


public class Hooks {

    public static TestDriver testDriver;
    public static Scenario scenario;


    @Before
    public void before(Scenario scenario) throws Throwable {
        //instantiate a test driver
        testDriver = new TestDriver();
        Hooks.scenario = scenario; //we do this so we can write pictures and text to the selenium logs easily
    }


    @After
    public void after() throws Throwable {
        //call quit on the TestDriver here and do any required logging.

        scenario.write("Timestamp at finish: " + HelperMethods.getDateInFormat("HH:mm:ss.SSS"));
        if (TestDriver.automator != null) {
            if (scenario.getStatus().equalsIgnoreCase("failed")) {
                TestDriver.automator.takeScreenshot(scenario);
            }
            testDriver.cleanup();
        }
    }

    public static void takeScreenshot() {
        TestDriver.automator.takeScreenshot(scenario);
    }
}
