package nz.co.enhance.StepDefs.Exercises.Web;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nz.co.enhance.Objects.Exercises.Web.Common.Header;
import nz.co.enhance.Objects.Exercises.Web.HomePage;
import nz.co.enhance.Objects.Exercises.Web.Login;
import nz.co.enhance.automationFramework.Automator;

import static org.junit.Assert.assertTrue;

public class LoginStepDefs {
    @Given("^I navigate to the login page (.*)$")
    public void i_navigate_to_the_URL(String url) {
        Automator.driver.navigate().to(url);
    }

    @When("^I click the login button$")
      public void i_click_the_login_button() {
            HomePage hp = new HomePage();
            hp.toLogin();
    }

    @When("^I login using \"([^\"]*)\" as email and \"([^\"]*)\" as password$")
    public void i_login_using_as_email_and_as_password(String email, String password) {
        Login login = new Login();
        login.toLoginUsingCredentials(email,password);
    }

    @Then("^I should verify the page contains \"([^\"]*)\"$")
    public void i_should_verify_the_page_contains(String expectedResult) {
        assertTrue(Automator.driver.getPageSource().contains(expectedResult));
    }
}
