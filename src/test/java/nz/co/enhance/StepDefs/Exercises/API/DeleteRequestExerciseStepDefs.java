package nz.co.enhance.StepDefs.Exercises.API;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import nz.co.enhance.StepDefs.Hooks;
import nz.co.enhance.automationFramework.ServiceClasses.DELETERequest;
import nz.co.enhance.automationFramework.ServiceClasses.HTTPRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DeleteRequestExerciseStepDefs {

    HTTPRequest request;

    @Given("^I send a Delete request to test api for user (\\d+)$")
    public void i_send_a_PUT_request_to_test_api_for_user(int user)  {
        String endpoint = "https://reqres.in/api/unknown/" + user;
        List<List<String>> headers = new ArrayList<>();
        headers.add(Arrays.asList("User-Agent", "Chrome/71.0.3578.98"));

        request = new DELETERequest(endpoint, headers);
        request.sendRequest();
        Hooks.scenario.write(request.logRequestAndResponse());
    }

    @Then("^I should see a Delete response code of (\\d+)$")
    public void i_should_see_a_delete_response_code_of(int resp) {
        assertEquals(resp, request.responseCode);
    }

    @And("^I should see the  response with empty value$")
    public void i_should_see_the_response_with_empty_value() {
        String results = "";
        boolean result = true;

        try{
           assertTrue("Response is not empty" , request.response.isEmpty());
           results = "Assertion passed";
       } catch (AssertionError ae)  {
           results = "Assertion failed - Response is not empty";
           result = false;
       }
        Hooks.scenario.write(results);
        assertTrue(result);
    }
}