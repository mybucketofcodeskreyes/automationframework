package nz.co.enhance.StepDefs.Exercises.Web;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nz.co.enhance.Objects.Exercises.Web.Common.BasePage;
import nz.co.enhance.Objects.Exercises.Web.CreateAccount;
import nz.co.enhance.Objects.Exercises.Web.Common.Header;
import nz.co.enhance.Objects.Exercises.Web.HomePage;
import nz.co.enhance.Objects.Exercises.Web.RegisterDone;
import nz.co.enhance.automationFramework.Automator;

import java.util.Random;

import static org.junit.Assert.assertTrue;

public class CreateUserStepDefs extends BasePage {
    public String address = "22 Fanshawe Street";

    @Given("^I navigate to this url (.*)$")
    public void i_navigate_to_this_url(String url) {
        Automator.driver.navigate().to(url);
    }

    @When("^I register$")
    public void i_register() {
        HomePage hp = new HomePage();
        hp.toRegister();
    }

    @When("^fill in the required information$")
    public void fill_in_the_required_information() {

        CreateAccount createAccount = new CreateAccount(address);
        createAccount.email.waitForElementToBeDisplayed(20);
        createAccount.email.sendKeys(createRandomChar() + "myemail@gmail.com");
        createAccount.password.sendKeys("1234567890pw");
        createAccount.confirmPassword.sendKeys("1234567890pw");
        createAccount.username.sendKeys(createRandomChar() + "myname");
        createAccount.firstname.sendKeys("Cdara");
        createAccount.lastname.sendKeys("Dasdavd");
        createAccount.gender.click();
        createAccount.dobDay.selectDropDownByVisibleText("5");
        createAccount.dobMonth.selectDropDownByVisibleText("September");
        createAccount.dobYear.sendKeys("1980");
        createAccount.phone.sendKeys("123123123");
        createAccount.country.click();
        createAccount.addressFinder.sendKeys(address);
        createAccount.addressSelection.waitForElementToBeDisplayed(20);
        createAccount.addressSelection.click();
        createAccount.postCode.waitForElementToExist(30);
        createAccount.closestDistrict.selectDropDownByVisibleText("Auckland - Auckland City");
        createAccount.terms.waitForElementToBeDisplayed(20);
        createAccount.terms.click();
        createAccount.submit.click();
    }

    @Then("^I should be able to create my account successfully$")
    public void i_should_be_able_to_create_my_account_successfully(){
        RegisterDone rd = new RegisterDone();
        rd.welcomeMsg.waitForElementToBeDisplayed(20);
        assertTrue(rd.welcomeMsg.getText().contains(", welcome to Trade Me!"));
    }
}
