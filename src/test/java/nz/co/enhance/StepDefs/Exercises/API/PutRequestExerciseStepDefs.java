package nz.co.enhance.StepDefs.Exercises.API;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nz.co.enhance.Objects.Examples.PostBody;
import nz.co.enhance.StepDefs.Hooks;
import nz.co.enhance.automationFramework.HelperClasses.JSONParser;
import nz.co.enhance.automationFramework.ServiceClasses.DELETERequest;
import nz.co.enhance.automationFramework.ServiceClasses.GETRequest;
import nz.co.enhance.automationFramework.ServiceClasses.HTTPRequest;
import nz.co.enhance.automationFramework.ServiceClasses.PUTRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PutRequestExerciseStepDefs {

    HTTPRequest request;

    @When("^I send a PUT request to test api$")
    public void i_send_a_PUT_request_to_test_api() {
        String endpoint = "https://reqres.in/api/users/2";
        List<List<String>> headers = new ArrayList<>();
        headers.add(Arrays.asList("User-Agent", "Chrome/71.0.3578.98"));
        headers.add(Arrays.asList("Content-Type", "application/json; charset=utf-8"));

        String body = "{\n" +
                "    \"name\": \"morpheus\",\n" +
                "    \"job\": \"zion resident\"\n" +
                "  }";

        request = new PUTRequest(endpoint, body ,headers);
        request.sendRequest();
        Hooks.scenario.write(request.logRequestAndResponse());
    }

    @Then("^I should see a PUT response code of (\\d+)$")
    public void i_should_see_a_PUT_response_code_of(int resp) {
        assertEquals(resp, request.responseCode);
    }

    @Then("^I should see the PUT response contains text (.*)$")
    public void i_should_see_the_PUT_response_contains_a_text_updatedAt(String resp) {
        assertTrue(request.response.contains(resp)); //
    }

    @Then("^the following fields name and job should have the following values:$")
    public void the_following_fields_name_and_job_should_have_the_following_values(DataTable putDataTbl) {
        String results = "";
        boolean result = true;

        List<List<String>> data = putDataTbl.cells(1); //turn the table into a list, ignoring the first row

        JSONParser j = new JSONParser(request.response);

        for (List<String> pairToCheck : data) {
            String field = pairToCheck.get(0);
            String value = pairToCheck.get(1);

            String actualValue = j.findValueAnyLevel(field);

            if (actualValue.equalsIgnoreCase(value)) {
                results += "PASSED: Field: " + field + " had the value " + value + " \n";
            } else {
                results += "FAILED: Field: " + field + " expected the value " + value + " but actually had: " + actualValue + "\n";
                result = false;
            }
        }

        Hooks.scenario.write(results);
        assertTrue(result);
    }
}
