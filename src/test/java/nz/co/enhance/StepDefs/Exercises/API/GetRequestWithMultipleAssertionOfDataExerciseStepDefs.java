package nz.co.enhance.StepDefs.Exercises.API;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nz.co.enhance.StepDefs.Hooks;
import nz.co.enhance.automationFramework.HelperClasses.JSONParser;
import nz.co.enhance.automationFramework.ServiceClasses.GETRequest;
import nz.co.enhance.automationFramework.ServiceClasses.HTTPRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class GetRequestWithMultipleAssertionOfDataExerciseStepDefs {

    HTTPRequest request;
    @When("^I send a GET request to test api with multiple assertion$")
    public void i_send_a_GET_request_to_test_api_with_multiple_assertion(){
        String endpoint = "https://reqres.in/api/users?page=2";
        List<List<String>> headers = new ArrayList<>();
        headers.add(Arrays.asList("User-Agent", "Chrome/71.0.3578.98"));

        request = new GETRequest(endpoint, headers);
        request.sendRequest();
        Hooks.scenario.write(request.logRequestAndResponse());
    }

    @Then("^I should see a response code which is (\\d+)$")
    public void i_should_see_a_response_code_which_is(int resp) {
        assertEquals(resp, request.responseCode);
    }

    @Then("^I should see the response contains multiple data:$")
    public void i_should_see_the_response_contains_multiple_data(DataTable dtable) {
        String results = "";
        boolean result = true;

        List<List<String>> data = dtable.cells(1); //turn the table into a list, ignoring the first row

        JSONParser j = new JSONParser(request.response);

        for (List<String> pairToCheck : data) {
            int node = Integer.parseInt(pairToCheck.get(0));
            String field = pairToCheck.get(1);
            String value = pairToCheck.get(2);
            String actualValue = j.getChildJSONArray("data").createMapFromJsonArray().get(node).get(field);
            if (actualValue.equalsIgnoreCase(value)) {
                results += "PASSED: Field: " + field + " had the value " + value + " \n";
            } else {
                results += "FAILED: Field: " + field + " expected the value " + value + " but actually had: " + actualValue + "\n";
                result = false;
            }
        }

        Hooks.scenario.write(results);
        assertTrue(result);
    }
}
