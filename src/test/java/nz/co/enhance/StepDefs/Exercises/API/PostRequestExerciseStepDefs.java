package nz.co.enhance.StepDefs.Exercises.API;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nz.co.enhance.Objects.Examples.PostBody;
import nz.co.enhance.StepDefs.Hooks;
import nz.co.enhance.automationFramework.ServiceClasses.HTTPRequest;
import nz.co.enhance.automationFramework.ServiceClasses.POSTRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PostRequestExerciseStepDefs {

    HTTPRequest request;

    @When("^I send a simple POST request with the following data:$")
    public void i_send_a_simple_POST_request_with_the_following_data(DataTable dataTbl){
        String endpoint = "https://reqres.in/api/users";

        List<List<String>> headers = new ArrayList<>();
        headers.add(Arrays.asList("Content-Type", "application/json; charset=utf-8"));
        headers.add(Arrays.asList("User-Agent", "Chrome/71.0.3578.98"));

        Map<String, String> data = dataTbl.asMap(String.class, String.class);

        PostBody postBody = new PostBody();
        postBody.name = data.get("name");
        postBody.job = data.get("job");

        request = new POSTRequest(endpoint, postBody.toJson(), headers);
        request.sendRequest();
        Hooks.scenario.write(request.logRequestAndResponse());
    }

    @Then("^I should see the Post response code is (\\d+)$")
    public void i_should_see_the_response_code_is(int resp) {
        assertEquals(resp, request.responseCode);
    }

   @Then("^I should verify the Post response contains text (.*) and (.*)$")
   public void i_should_verify_the_response_contains_the_text_and(String id, String createdAt) {
      String results = "";
      boolean result = true;

       try {
           assertTrue(request.response.contains(id));
           assertTrue(request.response.contains(createdAt));
           results = "Assertion passed";
       } catch (AssertionError error){
           results = "Assertion failed";
           result = false;

       }
       Hooks.scenario.write(results);
       assertTrue(result);
   }
}
