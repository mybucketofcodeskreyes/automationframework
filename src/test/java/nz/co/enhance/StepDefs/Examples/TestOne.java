package nz.co.enhance.StepDefs.Examples;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nz.co.enhance.Objects.Examples.GoogleSearchPage;
import nz.co.enhance.automationFramework.Automator;
import org.openqa.selenium.Keys;

import static org.junit.Assert.assertTrue;

public class TestOne {
   /* @Given("^I want to navigate to the URL (.*)$")
    public void i_want_to_navigate_to_the_URL(String url){
        System.out.println("THIS IS MY URL" + url);
        Automator.driver.navigate().to(url);
    }

    @When("^I will search for the term (.*)$")
    public void i_will_search_for_the_term_Happiness(String searchTerm) {
        GoogleSearchPage googleSearchPage = new GoogleSearchPage();
        googleSearchPage.searchBox.sendKeys(searchTerm);
        System.out.println("THIS IS WHAT IM SEARCHING FOR" + searchTerm);
        googleSearchPage.searchBox.sendKeys(Keys.ENTER);
    }

    @Then("^I should see there is a search result with the text (.*)$")
    public void i_should_see_there_is_a_search_result_with_the_text(String text) {
        GoogleSearchPage googleSearchPage = new GoogleSearchPage();
        googleSearchPage.searchResults.waitForElementToBeDisplayed(10); //make sure the search results are fully loaded
        System.out.println("THIS IS THE TEXT" + text);
        //assertTrue(googleSearchPage.searchResults.getText().contains(text));
    }*/
   @Given("^I want to navigate to the URL \"([^\"]*)\"$")
   public void i_want_to_navigate_to_the_URL(String url) {
       Automator.driver.navigate().to(url);
   }

    @When("^I will search for the term \"([^\"]*)\"$")
    public void i_will_search_for_the_term(String searchTerm) {
        GoogleSearchPage googleSearchPage = new GoogleSearchPage();
        googleSearchPage.searchBox.sendKeys(searchTerm);
        googleSearchPage.searchBox.sendKeys(Keys.ENTER);
    }

    @Then("^I should see there is a search result with the text \"([^\"]*)\" and \"([^\"]*)\"$")
    public void i_should_see_there_is_a_search_result_with_the_text_and(String arg1, String arg2) {
        GoogleSearchPage googleSearchPage = new GoogleSearchPage();
        googleSearchPage.searchResults.waitForElementToBeDisplayed(10); //make sure the search results are fully loaded
        assertTrue(googleSearchPage.searchResults.getText().contains(arg1));
        assertTrue(googleSearchPage.searchResults.getText().contains(arg2));
    }
}
